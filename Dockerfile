FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt ./
COPY requirements-server.txt ./ 

RUN pip install --no-cache-dir -r requirements.txt \
     && pip install --no-cache-dir -r requirements-server.txt

COPY . .
 

EXPOSE 8000

CMD ["gunicorn", "app:app", "-b", "0.0.0.0:8000"]

